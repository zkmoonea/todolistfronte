console.log("The script is connected");

// check off items by clicking
$("ul").on("click", "li", function(){
	$(this).toggleClass("completed");
});

// click on X to delete item
$("ul").on("click", "span", function(event){
	// following 'this' is the span element
	$(this).parent().fadeOut(500, function(){
		// following 'this' is parent
		$(this).remove();
	});
	event.stopPropagation();
});

$("input[type='text']").keypress(function(event){
	if(event.which === 13){
		// extract the value from the input box
		var todoText = $(this).val();
		// clear the input field
		$(this).val("");
		// create a new li and add to ul
		$("ul").append("<li><span><i class='fa fa-trash'></i></span> " + todoText + "</li>");
	}
});

// when clicked, fade in and out of the input box
$(".fa-plus").click(function(){
	$("input[type='text']").fadeToggle();
});